# **Yaru-Unity7** - Variant of Yaru for Unity7

Build and install Yaru-Unity7 themes from source.

This will install:

* GTK2 themes
* GTK3 themes
* Unity7 theme
* Yaru-unity icons

To try out the theme, follow the instructions:

Packages needed to build yaru-unity7:

* libgtk-3-dev
* sassc
* git
* meson
* sudo

**Install:**

# Clone this repository

```sh
git clone https://gitlab.com/ubuntu-unity/yaru-unity7.git
cd yaru-unity7
```

# Initialize build system (only required once per repo)

```sh
meson build

cd build
```

# Build and install

```sh
sudo ninja install
```

**Applying theme and icons:**

The theme can now be selected in Unity-Tweak-Tool.

**Uninstall:**

To uninstall Yaru-Unity7, simply run the following (Although some files won't be deleted succesfully)

```sh
cd yaru-unity7

cd build

sudo ninja uninstall
```

The remaining can be removed by doing this:

```sh
# deletes all GTK3-theme-folders named Yaru-unity, Yaru-unity-light, Yaru-unity-dark
sudo rm -rf /usr/share/themes/Yaru-unity*

# deletes icon-folder named Yaru-unity-light, Yaru-unity-dark
sudo rm -rf /usr/share/icons/Yaru-unity*
```

Once uninstalled you can reset your GTK and icon theme to the default settings in Unity-Tweak-Tool.

**Debian package**

Install `devscripts` and run `debuild`.

You'll find that a deb package has been generated in the upper directory.
